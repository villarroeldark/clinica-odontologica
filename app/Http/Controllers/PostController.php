<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
//aqui

use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        carbon::setlocale('es');
        $posts = Post::latest()->paginate(15);
        foreach($posts as $post){
            $post->setAttribute('user',$post->user);
            $post->setAttribute('added',Carbon::parse($post->created_at)->diffForHumans());
            $post->setAttribute('path',$post->slug);
        }
        return response()->json($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imageName = time().'.'.$request['file']->getClientOriginalExtension();
        //dd($imageName);
        $data = json_decode($request['form']);
        //dd($request->all());
        //dd($request['body']);0

        Post::create([
            'title' => $data->title,
            'slug' => Str::slug($data->title),
            'body' => $data->body,
            'name' => $data->name,
            'reserva' => $data->reserva,
            'hora' => $data->hora,
            'category_id' => $data->category,
            'user_id' => Auth::user()->id,
            'photo' => '/uploads/'.$imageName
        ]);
        $request['file']->move(public_path('uploads'),$imageName);

        return response()->json(['state'=>'exito de creacion de publicacion'],200);

        //error_log(json_decode($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //dd('respondiendo la solicitud del detalle de la publicacion');
        carbon::setlocale('es');
        return response()->json([
            'id'=>$post->id,
            'title'=>$post->title,
            'body'=>$post->body,
            'photo'=>$post->photo,
            'created_at'=>$post->created_at->diffForHumans(),
            'user'=>$post->user->name,
            'category'=>$post->category->name,
            'categoryid'=>$post->category->id,
            'comments_count'=> $post ->comments->count(),
            'reserva' => $post->reserva,
            'hora' => $post->hora,
            'comments'=>$this->AllCommentsPost($post->comments)
        ]);
    }
    public function AllCommentsPost($comments){
        $jsonComments = [];
        foreach($comments as $comment){

            array_push($jsonComments,[
                'id'=>$comment->id,
                'body'=>$comment ->body,
                'created_at'=>$comment ->created_at->diffForHumans(),
                'user' => $comment->user->name,
            ]);
        }
        return $jsonComments;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //dd($request->all());//por defecto
       // dd($request->all());
        if(Auth::user()->id == $post->user_id){
            if($request['file']){
                //dd('existe imagen');
                $imageName = time().'.'.$request['file']->getClientOriginalExtension();
                $request['file']->move(public_path('uploads'),$imageName);
                $post->photo ='/uploads/'.$imageName;
                

                $form = json_decode($request['form']);
                $post->title = $form->title;
                $post->name = $form->name;
                $post->reserva = $form->reserva;
                $post->hora = $form->hora;
                $post->category_id = $form->category;
                $post->body = $form->body;
                $post->slug = Str::slug($form->title);
           }else{
            
           
           }
        $post->save(); 
    
            return response()->json(['state'=>'editado con existo'], 200);
        }else{
            return response()->json(['state'=>'no le pertenece al usuario'], 401); 
        }
        }
       

    public function updateMyPost(Request $request){
        //dd('aqui tambien voy a actulizar');
        dd($request->all());
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //dd('aqui vamos a eliminar la publicacion');
        if(Auth::user()->id == $post->user_id){
            $post->delete();
            return response()->json(['state'=>'eliminado con exito'],200);

        }else{
            return response()->json(['state'=>'no le pertenece al usuario'], 401);
        }
        
    }
    //publicaciones del usuario logeado
    public function allPostForUser(){
        carbon::setlocale('es');
        $posts = Post::latest()->where('user_id',Auth::user()->id)->paginate(8);
        foreach($posts as $post){
            $post->setAttribute('user',$post->user);
            $post->setAttribute('added',Carbon::parse($post->created_at)->diffForHumans());
            $post->setAttribute('path',$post->slug);
        }
        return response()->json($posts);
    }

}
